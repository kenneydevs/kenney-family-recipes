from django.forms import ModelForm
from recipes.models import Recipe, RecipeStep, Ingredient

class CreateRecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]

# class AddRecipeStepForm(ModelForm):
#     class Meta:
#         model = RecipeStep
#         fields = [
#             "step_number",
#             "instruction",
#         ]

# class AddIngredientForm(ModelForm):
#     class Meta:
#         model = Ingredient
#         fields = [
#             "amount",
#             "food_item",
#         ]