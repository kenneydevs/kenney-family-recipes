from django.db import models
from django.contrib.auth.models import User

class Recipe(models.Model):
    title = models.CharField(max_length=100)
    picture = models.URLField(max_length=250, null=True)
    description = models.CharField(max_length=200)
    created_on = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(
        User,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    step_number = models.SmallIntegerField()
    instruction = models.TextField(max_length=400)
    recipe = models.ForeignKey(
        Recipe,
        related_name="steps",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["step_number"]

    def __str__(self):
        return "Step Number " + str(self.step_number)

class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["food_item"]

    def __str__(self):
        return self.amount + " of " + self.food_item