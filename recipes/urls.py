from django.urls import path
from recipes.views import list_recipes, show_recipe, create_recipe, edit_recipe, my_recipes_list

urlpatterns = [
    path("", list_recipes, name="list_recipes"),
    path("<int:id>/", show_recipe, name="show_recipe"),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit", edit_recipe, name="edit_recipe"),
    path("myrecipes/", my_recipes_list, name="my_recipe_list"),
]