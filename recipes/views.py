from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from django.contrib.auth.decorators import login_required
from recipes.forms import CreateRecipeForm

@login_required
def list_recipes(request):
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes
    }
    return render(request, "recipes/list_recipes.html", context)

@login_required
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe": recipe,
    }
    return render(request, "recipes/show_recipe.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = CreateRecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.creator = request.user
            recipe.save()
            return redirect("list_recipes")
    else:
        form = CreateRecipeForm()
    context = {
        "form": form,
    }
    return render(request, "recipes/create_recipe.html", context)

@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = CreateRecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = CreateRecipeForm(instance=recipe)
    context = {
        "recipe": recipe,
        "form": form,
    }
    return render(request, "recipes/edit_recipe.html", context)

@login_required
def my_recipes_list(request):
    recipes = Recipe.objects.filter(creator=request.user)
    context = {
        "recipes": recipes,
    }
    return render(request, "recipes/list_recipes.html", context)